# Default Permissions

BFFH has some standard permissions to assign to management and admin rights.

## UserSystem
* `bffh.users.manage` - Get ManageInterface in UserSystem (Access UserList)

## User
* `bffh.users.info` - Get InfoInterface for every User (Access UserInfos of other accounts)
* `bffh.users.admin` - Get AdminInterface for every User (Add/Remove Users, Reset Passwords)
