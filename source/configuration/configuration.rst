Configure FabAccess
===================

To run FabAccess you have to configure the Server(BFFH).
And add your Machines, Actors and Initiators.

In the current state of FabAccess we will no support database migration so all persistent data are in configuration files.

.. toctree::
   bffh_config.md
   user_config.md
   log_config.md
   default_permission.md
