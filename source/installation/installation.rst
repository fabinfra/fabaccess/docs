Install FabAccess
=================

FabAccess has an Server an Client structure.

So the Server and the Client must be installed seperatly.

Install Server
--------------
The Server can be installed over two recommended ways.

The easiest way is to use Docker with Docker-Compose.

To add youre own Plugins and Software you should use the Build way.

.. toctree::
   server_build.md
   server_docker.md

Install Client
--------------
The Client communicates with the server over an API.
So you can use any Client that support the current API Version of the Server.

We provide an Native Client for the following Platforms:
   * Android
   * iOS
   * UWP

For MacOS and Linux(GTK) we will provide a Client later in our Development.

.. toctree::
   client_store.md
   client_build.md

Installation Example for FabAccess Enviroment
---------------------------------------------

.. toctree::
   install_steps_ubuntu.md
