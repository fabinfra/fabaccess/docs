.. fab-access documentation master file, created by
   sphinx-quickstart on Thu Jul 16 19:46:22 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to FabAccess
======================================

FabAccess is a powerful system that enables the management and allocation of resources. With its flexible API, resource ownership and states can be effectively mapped, with each resource being assignable to a specific user. Furthermore, FabAccess provides the capability to track status changes of resources, facilitating precise monitoring and management.

A central aspect of FabAccess is the management of permissions, which allows for controlling access to specific resources. These permissions offer granular control over which user can access which resources, serving as a crucial tool for the security and efficiency of the system.

In this documentation, we will provide a comprehensive overview of FabAccess' features and usage possibilities, as well as detailed instructions for integration and utilization. We invite you to become acquainted with our system and discover its diverse capabilities.

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   concepts/concepts.rst
   installation/installation.rst
   configuration/configuration.rst
   usage/usage.rst
   concepts/concepts.rst
