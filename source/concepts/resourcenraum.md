# Beispiel Space
+ Spacename: Resourcenraum
+ InstanceURL: resourcenraum.fab-access.space
+ User-Passwort: secret
+ User-PIN: 1234

Der Beispielspace "Resourcenraum" ist ein Konstrukt, das entworfen wurde, um die verschiedenen Anforderungen und Konzepte verschiedener offener Werkstätten abzubilden. Auf diesem Space basieren auch die Tests für die API, mit denen die Funktionalität überprüft und Entwicklern ein Leitfaden gegeben werden kann.

## Users
### Admin - admin
User für Debugging und Testing

### User mit Aufgaben im Space
#### Sarah Barth - sbarth
- Adresse: Albrechtstrasse 73, 85003 Ingolstadt
- E-Mail: sbarth@resourcenraum.fab-access.space
- Rolle: Admin im Space mit allen Rechten
- Projekt: Management, Ausbildung, Workshop
- Card: 0000000000001

#### Claudia Neustadt - cneustadt
- Adresse: Bayreuther Strasse 25, 67661 Kaiserslautern Hohenecken
- E-Mail: cneustadt@resourcenraum.fab-access.space
- Rolle: Manage im Space für Metallbearbeitung
- Projekt: Management, Ausbildung
- Card: 0000000000002

#### Thomas Naumann - tneumann
- Adresse: Inge Beisheim Platz 6, 21339 Lüneburg
- E-Mail: tneumann@resourcenraum.fab-access.space
- Rolle: Manage im Space für Holzbearbeitung
- Projekt: Management, Ausbildung
- Card: 0000000000003

#### Maik Pfeiffer - mpfeiffer
- Adresse: Rudower Chaussee 15, 46238 Bottrop Batenbrock-Süd
- E-Mail: mpfeiffer@resourcenraum.fab-access.space
- Rolle: Manage im Space für Rapid Prototyping
- Projekt: Management, Ausbildung
- Card: 0000000000004

#### Katharina Abendroth - kabendroth
- Adresse: Storkower Strasse 34, 56332 Wolken
- E-Mail: kabendroth@resourcenraum.fab-access.space
- Rolle: Manage für alle Ressourcen zur Reparatur
- Projekt: Management
- Card: 0000000000005

#### Ralf Luft - rluft
- Adresse: An der Alster 3, 26721 Emden
- E-Mail: rluft@resourcenraum.fab-access.space
- Rolle: Manage für alle Ressourcen zur Reinigung
- Projekt: Management
- Card: 0000000000006

#### Sabine Faerber - sfaerber
- Adresse: Michaelkirchstr. 84, 34385 Bad Karlshafen
- E-Mail: sfaerber@resourcenraum.fab-access.space
- Rolle: Manage für User und Abrechnung
- Projekt: Management

#### Phillipp Blau - pblau
- Adresse: Schönwalder Allee 15, 09218 Neukirchen
- E-Mail: pblau@resourcenraum.fab-access.space
- Rolle: Workshop für neue Leute für unspezialisierte Ressourcen
- Projekt: Workshop
- Card: 0000000000007

### User die den Space nutzen
#### Michael Ziegler - mziegler
- Adresse: Leipziger Strasse 76, 33619 Bielefeld Niederdornberg
- E-Mail: mziegler@resourcenraum.fab-access.space
- Rolle: Neu im Space und hat noch keinen Account
- Card: 0000000000008

#### Leonie Fischer - lfischer
- Adresse: Hauptstrasse 10, 10115 Berlin Mitte
- E-Mail: lmueller@resourcenraum.fab-access.space
- Rolle: Nutzender des Space, hat Zugang zu allen Bereichen
- Card: 0000000000009

#### Julia Schneider - jschneider
- Adresse: Feldstrasse 5, 80333 München
- E-Mail: jschneider@resourcenraum.fab-access.space
- Rolle: Nutzende des Space, hat Zugang zur Metallbearbeitung

#### Felix Wagner - fwagner
- Adresse: Ludwig-Erhard-Strasse 20, 50679 Köln
- E-Mail: fwagner@resourcenraum.fab-access.space
- Rolle: Nutzender des Space, hat Zugang zur Holzbearbeitung
- Card: 000000000000A

#### Lara Schmidt - lschmidt
- Adresse: Am Wehrhahn 50, 40211 Düsseldorf
- E-Mail: lschmidt@resourcenraum.fab-access.space
- Rolle: Nutzende des Space, hat Zugang zum Rapid Prototyping

#### Lisa Meier - lmeier
- Adresse: Musterstraße 123, 12345 Musterstadt
- E-Mail: lmeier@resourcenraum.fab-access.space
- Rolle: Nutzende des Space, hat Zugang zum Rapid Prototyping
- Projekt: Horizon
- Card: 000000000000B

#### Tim Fischer - tfischer
- Adresse: Beispielweg 456, 54321 Beispielstadt
- E-Mail: tfischer@resourcenraum.fab-access.space
- Rolle: Nutzender des Space, hat Zugang zum Rapid Prototyping
- Projekt: Horizon

#### Niklas Becker - nbecker
- Adresse: Hauptplatz 8, 10178 Berlin Mitte
- E-Mail: nbecker@resourcenraum.fab-access.space
- Rolle: Keine zusätzlichen Berechtigungen
- Card: 000000000000D

## Resourcen
### Allgemein
#### Lötstation - solderstation
- Ansteuerung: 230V - Zwischenstecker
- Berechtigungen: Keine
- Abhängigkeit: Keine
- Trait: Powerable

#### 3D-Scanner - 3dscanner
- Ansteuerung: Keine
- Berechtigung: Keine
- Abhängigkeit: Keine
- Trait: Keine

#### Heißklebepistole - hotgluegun
- Ansteuerung: 12V DC Lock (feste Position)
- Berechtigung: Keine
- Abhängigkeit: Keine
- Trait: Doorable

#### Schraubendrehersatz - screwdriverset
- Ansteuerung: Keine
- Berechtigung: Keine
- Abhängigkeit: Keine
- Trait: Keine

#### Ringmaulschlüsselsatz - wrenchset
- Ansteuerung: Keine
- Berechtigung: Keine
- Abhängigkeit: Keine
- Trait: Keine

#### Akkuschrauber - electricscrewdriver
- Ansteuerung: 12V DC Lock (variable Position)
- Berechtigung: Elektrowerkzeuge
- Abhängigkeit: Keine
- Trait: Locatable, Lockable

### Stichsäge - jigsaw
- Ansteuerung: 12V DC Lock (variable Position)
- Berechtigung: Elektrowerkzeuge
- Abhängigkeit: Keine
- Trait: Locatable, Lockable

### Winkelschleifer - anglegrinder
- Ansteuerung: 12V DC Lock (variable Position)
- Berechtigung: Elektrowerkzeuge
- Abhängigkeit: Keine
- Trait: Locatable, Lockable

### Eingangstür - frontdoor
- Ansteuerung: 12V DC Summer
- Berechtigung: Keine
- Abhängigkeit: Keine
- Trait: Doorable (nur tmpOpen)

### Projektraum - projectroom
- Ansteuerung: 12V DC Summer
- Berechtigung: Keine
- Abhängigkeit: Keine
- Trait: Doorable

### Projektkiste (10 Stück) - projectbox
- Ansteuerung: 12V DC Lock (variable Position)
- Berechtigung: Jeweils pro Kiste
- Abhängigkeit: Keine
- Trait: Lockable, Locatable

### E-Scooter - escooter
- Ansteuerung: intern
- Berechtigung: Keine
- Abhängigkeit: Keine
- Trait: Lockable, Locatable
- Hint: Scooter

### Holzbearbeitung
#### Absaugung - centralsuction
- Ansteuerung: 230V - Schütz
- Berechtigung: Admin
- Abhängigkeit: Keine
- Trait: Powerable

#### Formatkreissäge - circularsaw
- Ansteuerung: 230V - Schütz
- Berechtigung: Holzbearbeitung
- Abhängigkeit: Absaugung
- Trait: Powerable

#### Bandsäge - bandsaw
- Ansteuerung: 400V - Schütz
- Berechtigung: Holzbearbeitung
- Abhängigkeit: Absaugung
- Trait: Powerable

#### Oberfräse - woodrouter
- Ansteuerung: 230V - Zwischenstecker
- Berechtigung: Felix Wagner
- Abhängigkeit: Keine
- Trait: Powerable

### Metallbearbeitung
#### Schweißgeräte - weldingmachine
- Ansteuerung: 230V - Zwischenstecker(intern), 12V DC Lock (feste Position)
- Berechtigung: Schweißen
- Abhängigkeit: Keine
- Trait: Powerable, Lockable

#### CNC - cnc
- Ansteuerung: 400V - Schütz
- Berechtigung: CNC
- Abhängigkeit: Keine
- Trait: Checkable

### Rapid Prototyping
#### 3D-Drucker FDM - 3dprinterfdm
- Ansteuerung: 230V - Zwischenstecker
- Berechtigung: 3D-Druck
- Abhängigkeit: Keine
- Trait: Powerable

#### 3D-Drucker Farm (10 Stück) - 3dprinterfarm0 bis 3dprinterfarm9
- Ansteuerung: 230V - Zwischenstecker
- Berechtigung: 3D-Druck
- Abhängigkeit: Keine
- Trait: Powerable

#### Lasercutter - lasercutter
- Ansteuerung: 230V - Zwischenstecker
- Berechtigung: Lasercut
- Abhängigkeit: Keine
- Trait: Powerable
- Bemerkung: Keine Disclose Berechtigung und Alias

## Terminals
curved25519 Zertifikate

### CNC - cnc
- Resource: CNC

### 3D-Drucker Farm - farm
- Resource: 3D-Drucker Farm

### Lasercutter - lasercutter
- Resource: Lasercutter
- Bemerkung: Alias QR Code

### Warenausgabe - goodsissue
- Resource: Schraubendrehersatz, Ringmaulschlüsselsatz