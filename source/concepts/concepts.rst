Concepts of FabAccess
=================

FabAccess utilizes several key concepts to facilitate the process of resource allocation and provide flexibility where needed to accommodate customization.

.. toctree::
   claims.md
   traits.md
   measurements.md
   projects.md
   terminals.md