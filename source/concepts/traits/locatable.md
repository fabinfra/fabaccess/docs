# Locatable
Der Trait "Locatable" ermöglicht die Identifizierung von Ressourcen, wie beispielsweise Schließfächer oder 3D-Drucker in einer Druckerfarm. Dabei kann entweder eine kurzfristige Identifikation abgegeben werden oder die Identifizierung dauerhaft gesetzt werden.

## OID
`1.3.6.1.4.1.61783.612.1.5`

## States
```mermaid
stateDiagram
    [*] --> IDLE
    IDLE --> ACTIVE: setActive
    IDLE --> ACTIVE: identify
    ACTIVE --> IDLE: setIdle
    ACTIVE --> IDLE: AUTO
```