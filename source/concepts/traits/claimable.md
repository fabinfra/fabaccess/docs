# Claimable
Der Trait "Claimable" stellt einen Sonderfall dar, da er dazu dient, dass sich ein Nutzer über den Claim-Zustand einer Ressource informieren kann.

Nutzer können auf diesem Trait keine Aktionen ausführen, sondern lediglich den Zustand abfragen.

## OID
`1.3.6.1.4.1.61783.612.1.0`