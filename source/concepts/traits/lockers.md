# Lockers
"Lockers" ist einer der komplexeren Traits, mit dem Schlösser von Ressourcen genauer abgebildet werden können. Der Nutzer kann die Ressource nicht in jedem Zustand zurückgeben; erst wenn alles korrekt zurückgegeben wurde, kann die Ressource auch wieder gesperrt werden.

Die Zustandsänderungen zwischen den vom Nutzer verwendbaren Übergängen können von einem Initiator herbeigeführt werden.

## OID
`1.3.6.1.4.1.61783.612.1.4`

## States
```mermaid
stateDiagram
    [*] --> locked
    locked --> unlocked: unengage
    unlocked --> locked: engage
    unlocked --> open: AUTO
    open --> unengaged: AUTO
    unengaged --> locked: engage
    unengaged --> open: AUTO