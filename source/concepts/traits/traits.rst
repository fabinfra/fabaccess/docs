Traits
=================
Traits bieten die Möglichkeit, den Zustand von Ressourcen zu ändern. Ressourcen können mehrere Traits besitzen und diese kombiniert nutzen.

Mit Traits erhalten Nutzer Zugriff auf die Ressource, nachdem sie einen Claim erhalten haben. Dabei können Traits verwendet werden, um Ressourcen aus bestehenden Traits zusammenzusetzen oder spezifische Traits zu implementieren.

Um eine optimale Anzeige der Traits für Nutzer in Clients zu ermöglichen, kann einer Ressource ein "Hint" hinzugefügt werden. Dieser ermöglicht es einem Client, eine verbesserte Ansicht der Ressource für Nutzer zu generieren.

Traits werden anhand einer OID (Object Identifier) bereitgestellt. In FabAccess gibt es bereits vordefinierte Traits für grundlegende Funktionen, mit denen viele Zustände von Ressourcen abgebildet werden können.

.. toctree::
   claimable.md
   powerable.md
   doorable.md
   lockers.md
   checkable.md
   locatable.md