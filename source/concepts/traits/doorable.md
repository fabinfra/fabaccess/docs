# Doorable
Der Trait "Doorable" ermöglicht die Abbildung von Türen oder anderen Schließsystemen. Dabei besteht die Möglichkeit, kurzzeitige Öffnungen zu realisieren. Die genaue Zeitdauer, für die die Ressource geöffnet wird, wird dabei vom Server bestimmt.

## OID
`1.3.6.1.4.1.61783.612.1.2`

## States
```mermaid
stateDiagram
    [*] --> CLOSED
    CLOSED --> OPEN: unlock
    OPEN --> CLOSED: lock
    CLOSED --> tempOPEN: unlocktemp
    tempOPEN --> OPEN: unlock
    tempOPEN --> CLOSED: lock
    tempOPEN --> CLOSED: AUTO
```