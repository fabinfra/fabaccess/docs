# Claim Concept

The claim concept serves as an abstraction for allocating a resource in FabAccess. Its primary purpose is to manage the methods by which users gain access to a resource and share that access among themselves.

## Claims
A "claim" in FabAccess represents the granted access to a resource.

The flexibility of claims enables effective support for various scenarios of resource access. For instance, multiple users can simultaneously claim access to a resource, which is particularly advantageous in collaborative working environments. Additionally, the ability to register in a queue with an interest provides a practical solution for situations where resource availability is limited, and users must wait to access it.

Another significant aspect of claims is their capability to transfer or lend resources between users. These functionalities facilitate a flexible utilization of resources and foster collaboration among users. For example, a user who no longer requires a resource can transfer it to another user in need, or an instructor can temporarily grant access to a resource to a trainee to acquire specific skills.

## Interest
An integral part of the claim concept is "interest," which represents reservations that can be either time-based or queue-based. These interests allow users to secure future access to a resource, either based on a predefined time or in the order of entry into the queue.

With an interest, the user signals their interest in a particular resource to the system. At the next opportunity, the user either receives a claim to the resource or has the option to upgrade their interest to a claim. This flexibility enables users to adapt to their needs and respond to changes in resource availability.

## Notify
The final element of the claim concept is the "notify." Through the notify, users can view the status of a resource and receive notifications about changes.

The notify allows users to retrieve the current state of a resource and respond to changes as needed. This provides an essential means of monitoring resource status and reacting promptly to relevant events.

By registering for notifications of state changes, users can effectively interact with resources and ensure they are informed about important developments at all times.