# NFC Cards (FabFire)
## Functional Principle
### Basic
Card authentication using NXP/MiFare DESFire cards.
These cards have the ability to restrict access for data on the cards using symmetric encryption and using a keyed Diffie-Hellman to prevent eavesdropping by any relaying party.
A card has several "applications", containing up to 32 files. A file can be read or written. Both kinds of access can be restricted to parties knowing a PSK, on a file-to-file basis.

### Files
The current system uses File 0001 through File 0003:

**File 0001** allows public (i.e. unauthenticated) read access and contains the Strings
`FABACCESS`, `DESFIRE`, and `1.0` as packed list of UTF-8 encoded zero-terminated strings.

Examples:
- `FABACCESS\0DESFIRE\01.0\0`

This file serves an identifier, allowing a server to verify if it is able to use this card.

**File 0002** allows public read access and contains:
An URL-encoded name of the issuing lab as URN in the format 
`urn:fabaccess:lab:<labname>`

Examples:
- `urn:fabaccess:lab:innovisionlab`
- `urn:fabaccess:lab:Bibliothek%20Neustadt%20Makerspace`
- `urn:fabaccess:lab:Offene%20Werkstatt%20M%C3%A4rz`

A valid IRI pointing towards the bffh instance running for this lab. This bffh SHOULD be reachable from the internet. Using private use IP addresses or IRIs that resolve to such may be necessary for labs behind restrictive firewalls or due to local policy.
The IRI MUST use the "fabaccess" scheme, and SHOULD NOT contain an userinfo, path, query, or fragment part.
Examples: 
- `fabaccess://innovisionlab.de/`
- `fabaccess://192.168.178.65`
- `fabaccess://fabaccess-server.localnet`

A zero-terminated list of UTF-8 encoded IRIs giving contact options to notify the issuer
or owner in case the card has been lost. Issuers SHOULD set one value on card creation and MAY allow card owners to change or add values of their choosing.
Examples:
- `mailto:lostcard@innovisionlab.de`
- `https://innovisionlab.de/lostcard`
- `https://werkstatt-märz.decardlost.php?action=submitcardlost`

**File 0003** allows public access or access using a key, at the issuers option.
It contains a token that can be used by the home server of the card owner to identify the card owner. The format of the token MUST NOT be relied on by any party except the home server.

## FabFire Specification
Application Identifier (AID): `0x464142`

## NFC Implementation
C# - https://gitlab.com/fabinfra/fabaccess/nfc
Rust - https://gitlab.com/fabinfra/fabaccess/nfc_rs

## Tools
Tool for provisioning new cards for use with the FabAccess card system.
https://gitlab.com/fabinfra/fabaccess/FabFire-Provisioning-Tool

FabFire adapter translates mqtt messages from the reader hardware to api
https://gitlab.com/fabinfra/fabaccess/fabfire_adapter