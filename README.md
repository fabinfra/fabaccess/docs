# fabaccess-docs

Documentation for the fabaccess project.
You can find the rendered docs at: https://fab-access.readthedocs.io/ or https://docs.fab-access.org 

Checkout the branches of this repo for the correct documentation for your version.

# ! Warning !
This project is not maintained anymore and is going to be archived from now on (14.12.2024). You will find all recent installation and configuration documentations at the BookStack documentation space https://docs.fab-access.org